package com.vantec.jobs.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "documentHeader")
public class DocumentHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "documentReferenceCode")
	private String documentReferenceCode;
	
	@Column(name = "tanum")
	private String tanum;
	
	
	@Column(name = "benum")
	private String benum;
	
	@Column(name = "bwlvs")
	private String bwlvs;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requirementHeaderId", referencedColumnName = "id")
	private DocumentHeader requirementHeaderId;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduledWorkId", referencedColumnName = "id")
	private ScheduledWork scheduleWork;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@Column(name = "pickType")
	private String pickType;
	
	@Column(name = "allocated")
	private Boolean allocated;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;
	
	@OneToMany(mappedBy = "documentHeader")
	private List<DocumentBody> documentBodies = new ArrayList<DocumentBody>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReferenceCode() {
		return documentReferenceCode;
	}

	public void setDocumentReferenceCode(String documentReferenceCode) {
		this.documentReferenceCode = documentReferenceCode;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public String getBenum() {
		return benum;
	}

	public void setBenum(String benum) {
		this.benum = benum;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	public List<DocumentBody> getDocumentBodies() {
		return documentBodies;
	}

	public void setDocumentBodies(List<DocumentBody> documentBodies) {
		this.documentBodies = documentBodies;
	}

	public String getBwlvs() {
		return bwlvs;
	}

	public void setBwlvs(String bwlvs) {
		this.bwlvs = bwlvs;
	}

	public DocumentHeader getRequirementHeaderId() {
		return requirementHeaderId;
	}

	public void setRequirementHeaderId(DocumentHeader requirementHeaderId) {
		this.requirementHeaderId = requirementHeaderId;
	}

	public String getPickType() {
		return pickType;
	}

	public void setPickType(String pickType) {
		this.pickType = pickType;
	}

	public ScheduledWork getScheduleWork() {
		return scheduleWork;
	}

	public void setScheduleWork(ScheduledWork scheduleWork) {
		this.scheduleWork = scheduleWork;
	}

	public Boolean getAllocated() {
		return allocated;
	}

	public void setAllocated(Boolean allocated) {
		this.allocated = allocated;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getTanum() {
		return tanum;
	}

	public void setTanum(String tanum) {
		this.tanum = tanum;
	}
	
	
	
}
