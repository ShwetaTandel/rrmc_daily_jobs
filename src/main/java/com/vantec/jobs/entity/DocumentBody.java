package com.vantec.jobs.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "documentBody")
public class DocumentBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "document_header_id", referencedColumnName = "id")
	private DocumentHeader documentHeader;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "qty_expected")
	private Integer qtyExpected;
	
	@Column(name = "qty_transacted")
	private Integer qtyTransacted;
	
	@Column(name = "date_created")
	private Date dateCreated;
	
	@Column(name = "last_updated_by")
	private String updatedBy;

	@Column(name = "last_updated")
	private Date dateUpdated;
	
	@OneToMany(mappedBy = "documentBody")
	private List<DocumentDetail> documentDetails = new ArrayList<DocumentDetail>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DocumentHeader getDocumentHeader() {
		return documentHeader;
	}

	public void setDocumentHeader(DocumentHeader documentHeader) {
		this.documentHeader = documentHeader;
	}
	

	public Integer getQtyExpected() {
		return qtyExpected;
	}

	public void setQtyExpected(Integer qtyExpected) {
		this.qtyExpected = qtyExpected;
	}

	public Integer getQtyTransacted() {
		return qtyTransacted;
	}

	public void setQtyTransacted(Integer qtyTransacted) {
		this.qtyTransacted = qtyTransacted;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public List<DocumentDetail> getDocumentDetails() {
		return documentDetails;
	}

	public void setDocumentDetails(List<DocumentDetail> documentDetails) {
		this.documentDetails = documentDetails;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	

}
