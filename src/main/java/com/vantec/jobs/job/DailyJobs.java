package com.vantec.jobs.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vantec.jobs.service.JobService;

@Component
public class DailyJobs {

	private static Logger logger = LoggerFactory.getLogger(DailyJobs.class);

	@Autowired
	JobService jobService;

	@Scheduled(cron = "0 0 16  * * ?")
	public void callDailyReports() {
		jobService.callDailyReports();
	}

	public void callPartReconcilationJob() {

	}

	public void callPartUpdateJob() {

	}

	public void callPickSummaryJob() {

	}

	public void callReconcilationReport() {

	}

	public void callTrailerReport() {

	}
}
