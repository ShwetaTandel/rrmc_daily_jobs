package com.vantec.jobs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.jobs.service.JobService;

@RestController
@RequestMapping("/")
public class JobsController {

	private static Logger logger = LoggerFactory.getLogger(JobsController.class);

	@Autowired
	JobService jobService;

	@RequestMapping(value = "/callJobs", method = RequestMethod.GET)
	public ResponseEntity<Boolean> callJobs() {

		try {
			logger.info("callJobs called..");
			jobService.callPartUpdateJob();
			return new ResponseEntity<Boolean>(HttpStatus.OK);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
