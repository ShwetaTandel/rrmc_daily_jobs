package com.vantec.jobs.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.jobs.controller.JobsController;
import com.vantec.jobs.entity.InventoryMaster;
import com.vantec.jobs.entity.TransactionHistory;
import com.vantec.jobs.repository.InventoryMasterRepository;
import com.vantec.jobs.repository.TransactionHistoryRepository;

@Service("dailyReportsInfo")
public class DailyReportsImpl implements DailyReports {
	private static Logger logger = LoggerFactory.getLogger(DailyReportsImpl.class);

	@Autowired
	InventoryMasterRepository invRepo;

	@Autowired
	TransactionHistoryRepository transHistRepo;

	// sqlStr = "SELECT a.part_id, a.serial_reference AS
	// serial_reference, b.part_number AS part_number,
	// b.part_description AS part_description, b.conversion_factor AS
	// conversion_factor, a.inventory_qty AS inventory_qty,
	// a.allocated_qty AS allocated_qty, a.parent_tag_id AS
	// parent_tag_id, substring(a.stock_date,1,10) AS stock_date,
	// c.location_code AS location_code, c.location_type_id AS
	// location_type_id, c.current_location_status_id AS
	// current_location_status_id, d.tag_reference as tag_reference,
	// e.inventory_status_code AS inventory_status_code FROM
	// vantec.inventory_master a LEFT JOIN vantec.part b ON
	// a.part_id=b.id LEFT JOIN vantec.location c ON
	// a.current_location_id = c.id LEFT JOIN vantec.tag d ON
	// a.parent_tag_id=d.id LEFT JOIN vantec.inventory_status e ON
	// c.current_location_status_id=e.id WHERE a.inventory_qty>0 ORDER
	// BY a.part_id";
	// ResultSet rs = st.executeQuery(sqlStr);
	public void writeInventoryCSV(String fileName) {
		String lineStr = "";
		try {
			new File(fileName).delete();
			FileWriter wf = new FileWriter(new File(fileName));
			lineStr = "Part_Number,Description,serial_reference,location_code,inventory_qty,allocated_qty,inventory_status_code,tag_reference,stock_date"
					+ "\r\n";
			wf.write(lineStr);
			List<InventoryMaster> inventory = invRepo.findAllInventory();
			for (InventoryMaster invMaster : inventory) {
				try {
					String serialReference = "";
					String tagReference = "";
					String partDescription = "";
					String locationCode = "";
					String stockDate = "";
					String invstatus = "";
					int invQty = 0;
					int allocatedQty = 0;
					int conversionFactor = 0;
					if (invMaster.getConversionFactor().intValue() == 0) {
						conversionFactor = 1000;
					} else {
						conversionFactor = invMaster.getConversionFactor().intValue();
					}
					if (invMaster.getInventoryQty().intValue() != 0)
						invQty = invMaster.getInventoryQty().intValue() / conversionFactor;
					if (invMaster.getAllocatedQty().intValue() != 0)
						allocatedQty = invMaster.getAllocatedQty().intValue() / conversionFactor;
					if (invMaster.getSerialReference() == null || invMaster.getSerialReference().equals("null")) {
						serialReference = "";
					} else {
						serialReference = invMaster.getSerialReference();
					}
					// replace escape characters
					if ((invMaster.getPart().getPartDescription() == null)
							|| (invMaster.getPart().getPartDescription().equals("null"))) {
						partDescription = "";
					} else {
						partDescription = invMaster.getPart().getPartDescription();
					}
					partDescription = partDescription.replaceAll("\t", "");
					partDescription = partDescription.replaceAll("\n", "");
					partDescription = partDescription.replaceAll("\r", "");
					partDescription = partDescription.replaceAll("'", " ");
					partDescription = partDescription.replaceAll(",", " ");
					partDescription = partDescription.replaceAll("#", " ");
					partDescription = partDescription.replaceAll("\"", " ");
					if (invMaster.getParentTag() != null && invMaster.getParentTag().getTagReference() != null
							&& (!invMaster.getParentTag().getTagReference().equals("null"))) {
						tagReference = invMaster.getParentTag().getTagReference();
					}
					if (invMaster.getCurrentLocation() != null
							&& invMaster.getCurrentLocation().getLocationCode() != null) {
						locationCode = invMaster.getCurrentLocation().getLocationCode();
					}
					if (invMaster.getInventoryStatus() != null
							&& invMaster.getInventoryStatus().getInventoryStatusCode() != null) {
						invstatus = invMaster.getInventoryStatus().getInventoryStatusCode();
					}
					if (invMaster.getStockDate() != null) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						sdf.setLenient(false);
						stockDate = sdf.format(invMaster.getStockDate());

					}
					lineStr = invMaster.getPartNumber() + "," + partDescription.trim() + "," + serialReference + ","
							+ locationCode + "," + invQty + "," + allocatedQty + "," + invstatus + "," + tagReference
							+ "," + stockDate + "\r\n";
					wf.write(lineStr);
				} catch (Exception e) {
					logger.info(e.getMessage());
				}
			}
			wf.close();
		} catch (IOException io) {
			logger.info(io.getMessage());
		}
	}

	// Carset Pick History to CSV format
	// sqlStr = "SELECT a.id AS id, a.associated_document_reference AS
	// associated_document_reference, substring(a.date_created,1,19) AS
	// date_created, a.from_location_code AS from_location_code,
	// a.part_number AS part_number, a.ran_or_order AS ran_or_order,
	// a.comment AS comment, a.serial_reference AS serial_reference,
	// a.short_code AS short_code, a.txn_qty AS txn_qty,
	// a.last_updated_by AS last_updated_by, b.conversion_factor AS
	// conversion_factor, c.benum AS benum FROM
	// vantec.transaction_history a LEFT JOIN vantec.part b ON
	// a.part_number=b.part_number LEFT JOIN vantec.document_header c ON
	// a.ran_or_order=tanum WHERE a.short_code='CSET' and
	// date(a.date_created) >=curdate() ORDER BY a.date_created";
	public void writeCarsetPickCSV(String fileName) {
		String lineStr = "";

		try {
			new File(fileName).delete();
			FileWriter wf = new FileWriter(new File(fileName));
			lineStr = "id,associated_document_reference,date_created,from_location_code,part_number,ran_or_order,car_set,serial_reference,short_code,txn_qty,last_updated_by"
					+ "\r\n";

			wf.write(lineStr);

			List<TransactionHistory> hists = transHistRepo.findAllWithShortCodeAndCreatedYesterday("CSET");
			// ResultSet rs = st.executeQuery(sqlStr);
			for (TransactionHistory hist : hists) {
				try {
					String comment = "";
					int txnQty = 0;
					int convFactor = 1000;
					if (hist.getPart().getConversionFactor() != 0) {
						convFactor = hist.getPart().getConversionFactor();
					}
					if (hist.getTxnQty().intValue() != 0) {
						txnQty = hist.getTxnQty().intValue() / convFactor;
					}
					if (hist.getComment() == null) {
						if (hist.getHeader() != null)
							comment = hist.getHeader().getBenum();
					} else {
						comment = hist.getComment();
					}
					lineStr = hist.getId() + "," + hist.getAssociatedDocumentReference() + "," + hist.getDateCreated()
							+ "," + hist.getFromLocationCode() + "," + hist.getPartNumber() + "," + hist.getRanOrOrder()
							+ "," + comment + "," + hist.getSerialReference() + "," + hist.getShortCode() + "," + txnQty
							+ "," + hist.getUpdatedBy() + "\r\n";

					wf.write(lineStr);
				} catch (Exception e) {
					logger.info(e.getMessage());
				}
			}
			wf.close();
		} catch (IOException io) {
			logger.error(io.getMessage());
		}

	}

}
