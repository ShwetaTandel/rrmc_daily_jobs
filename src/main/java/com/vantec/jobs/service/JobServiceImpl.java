package com.vantec.jobs.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.jobs.repository.JobDefinitionRepository;

@Service("jobService")
public class JobServiceImpl implements JobService {
	private static Logger logger = LoggerFactory.getLogger(JobServiceImpl.class);
	@Autowired
	DailyReports dailyReports;

	@Autowired
	MailService mailService;
	
	@Autowired
	JobDefinitionRepository jobDefnRepo;
	
	@Autowired
	PartsJob partsJob;
	
	

	@Override
	public void callDailyReports() {
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		List<String> fileList = new ArrayList<String>();
		System.out.println("Writing CSV inventory..");
		//dailyReports.writeInventoryCSV("C:\\RRMCJobs\\DailyReport\\INVENTORYREPORT.CSV");
		System.out.println("Writing CSV carset..");
		dailyReports.writeCarsetPickCSV("C:\\RRMCJobs\\DailyReport\\CARSETPICKREPORT.CSV");
		fileList.add("C:\\RRMCJobs\\DailyReport\\INVENTORYREPORT.CSV");
		fileList.add("C:\\RRMCJobs\\DailyReport\\CARSETPICKREPORT.CSV");
		mailService.sendMail("shweta-tandel.ce@vantec-gl.com", fileList,
				"RRMC Inventory " + sdf.format(dt));
		System.out.println("Sending emails.....");
		fileList.clear();
		fileList.add("C:\\RRMCJobs\\DailyReport\\INVENTORYREPORT.CSV");
		fileList.add("C:\\RRMCJobs\\DailyReport\\CARSETPICKREPORT.CSV");
		mailService.sendMail(jobDefnRepo.findByJobName("DAILY_REPORT").getDistributionList(), fileList,
				"RRMC Carset Pick " + sdf.format(dt));

	}

	@Override
	public void callPartReconcilationJob() {
		
	}

	@Override
	public void callPartUpdateJob() {
	try {
			
			// copy file ZIF_LSP_STOCK.OUT from shared cddata to local path
			//partsJob.copyFile("\\172.18.21.5\\prod\\ZIF_LSP_MM.OUT", "C:\\RRMCJobs\\PartUpdate\\ZIF_LSP_STOCK.OUT");
			logger.info("Copy file successful");

			partsJob.writeXMLFile();

			/*
			partsJob.partInsert();

			// PART update qa
			partsJob.partUpdateQA();

			// PART updated vendor part update
			partsJob.partUpdateVendorPart();

			// PART update location type
			partsJob.partUpdateLocationType();
			new File("Y:\\prod\\ZIF_LSP_MM.OUT").delete();
			List<String> fileList = new ArrayList<String>();
			fileList.add("C:\\RRMCJobs\\PartUpdate\\ZIF_LSP_STOCK.OUT");
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			mailService.sendMail(jobDefnRepo.findByJobName("PART_UPDATE").getDistributionList(), fileList, "RRMC_Parts_File "+sdf.format(dt));*/
		} catch (Exception ex) {
			
			System.out.println(ex);

		}

	}

	@Override
	public void callPickSummaryJob() {

	}

	@Override
	public void callReconcilationReport() {

	}

	@Override
	public void callTrailerReport() {

	}

	
}
