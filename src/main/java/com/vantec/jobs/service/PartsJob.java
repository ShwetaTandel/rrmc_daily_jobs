package com.vantec.jobs.service;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

public interface PartsJob {

	public void dropTable(Connection conn);

	public void copyFile(String from, String to) throws IOException;


	public void sqlDropTable(String fileName);

	public void sqlCreateTable(String fileName, String[] detailArray);

	public void writeXMLFile();

	public void sqlInsertData(String fileName, String sqlStr, String uniqueId);

	public void partInsert();

	public void partUpdateQA();

	public void partUpdateVendorPart();

	public void partUpdateLocationType();

}
