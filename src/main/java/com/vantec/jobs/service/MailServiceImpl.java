package com.vantec.jobs.service;

import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.jobs.repository.JobDefinitionRepository;




@Service("mailService")
public class MailServiceImpl implements MailService{
	
	@Autowired
	JobDefinitionRepository jobDefn ;
	
	   
	   public void sendMail(String toAddress,List<String> fileList,String subject){
	  		
		   Properties properties = System.getProperties();
	       properties.put("mail.smtp.auth", "false");
	       properties.put("mail.smtp.host","172.16.10.196");
	       properties.put("mail.smtp.port", 25);
	       Session session = Session.getDefaultInstance(properties);

	       session = Session.getInstance(properties);

	  		try {

	  			//Compose the message  
	  			Message message = new MimeMessage(session);
	  			message.setFrom(new InternetAddress("accounts@vanteceurope.com"));
	  			message.setRecipients(Message.RecipientType.TO,
	  					InternetAddress.parse(toAddress));
	  			message.setSubject(subject);
	  			message.setText("This was prepared by an automated job!!");
	  			
	  			//Attach attachments
	  			Multipart multipart = new MimeMultipart();
	  			for(String file:fileList){
		  			MimeBodyPart messageBody = new MimeBodyPart();
		  			messageBody = new MimeBodyPart();
		  			DataSource source = new FileDataSource(file);
		  			messageBody.setDataHandler(new DataHandler(source));
		  			messageBody.setFileName(file);
		  			multipart.addBodyPart(messageBody);
	  			}

	  			//send the message  
	  			message.setContent(multipart);
	  			Transport.send(message);

	  		} catch (MessagingException e) {
	  			e.printStackTrace();
	  		}
	   }
	   
	  
	   
	  
}
