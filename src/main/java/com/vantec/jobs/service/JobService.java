package com.vantec.jobs.service;

public interface JobService {
	
	public void callDailyReports();
	public void callPartReconcilationJob();
	public void callPartUpdateJob();
	public void callPickSummaryJob();
	public void callReconcilationReport();
	public void callTrailerReport();
	
	
	 

}
