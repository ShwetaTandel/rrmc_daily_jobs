package com.vantec.jobs.service;

import java.util.List;

public interface MailService {
	public void sendMail(String toAddress,List<String> fileList,String subject);

}
