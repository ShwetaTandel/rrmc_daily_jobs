package com.vantec.jobs.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.jobs.entity.InventoryMaster;

public interface InventoryMasterRepository extends JpaRepository<InventoryMaster, Serializable>{
	
	@Query("Select t from InventoryMaster t where t.inventoryQty > 0 Order By part")
	List<InventoryMaster> findAllInventory();

}

